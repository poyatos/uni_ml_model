import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from collections import Counter
from scipy.stats import randint
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score, make_scorer
from sklearn.model_selection import RandomizedSearchCV
from sklearn.externals import joblib
from imblearn.pipeline import make_pipeline, Pipeline
from imblearn.over_sampling import SMOTE
from sklearn.preprocessing import OneHotEncoder

# config
skip_fitting: bool = True
n_estimators = 150
model_dump_file_name = "model_dump.pkl"

# Data Import
dataset = pd.read_csv(
    'https://raw.githubusercontent.com/saschaschworm/big-data-and-data-science/' +
    'master/datasets/prediction-challenge/dataset.csv',
    index_col='identifier', parse_dates=['date'])

prediction_dataset = pd.read_csv(
    'https://raw.githubusercontent.com/saschaschworm/big-data-and-data-science/' +
    'master/datasets/prediction-challenge/prediction-dataset.csv',
    index_col='identifier', parse_dates=['date'])

# Data Visualization
cat_columns = ['job', 'marital_status', 'education', 'credit_default', 'housing_loan', 'personal_loan',
               'communication_type', 'success']

fig, axs = plt.subplots(3, 3, sharex=False, sharey=False, figsize=(20, 15))

counter = 0
for cat_column in cat_columns:
    value_counts = dataset[cat_column].value_counts()

    trace_x = counter // 3
    trace_y = counter % 3
    x_pos = np.arange(0, len(value_counts))

    axs[trace_x, trace_y].bar(x_pos, value_counts.values, tick_label=value_counts.index)

    axs[trace_x, trace_y].set_title(cat_column)

    for tick in axs[trace_x, trace_y].get_xticklabels():
        tick.set_rotation(90)

    counter += 1

plt.show()

num_columns = ['n_contacts_before', 'days_since_last_contact', 'n_contacts_campaign', 'duration', 'age']

fig, axs = plt.subplots(2, 3, sharex=False, sharey=False, figsize=(20, 15))

counter = 0
for num_column in num_columns:
    trace_x = counter // 3
    trace_y = counter % 3

    axs[trace_x, trace_y].hist(dataset[num_column])

    axs[trace_x, trace_y].set_title(num_column)

    counter += 1

plt.show()


# Feature Engineering
# Create some new features based on the given features
# or enrich the dataset with features from datasets.

def getWeekday(n):
    return n.weekday()


def getDay(n):
    return n.day


def getMonth(n):
    return n.month


def getYear(n):
    return n.year


def getAge(n):
    return (n - n % 5) / 5


def getCampaign(n):
    return (n - n % 2) / 2


def getDaysLastContact(n):
    return (n - n % 3) / 3


def getDuration(n):
    return (n - n % 150) / 150


def isContactBefore(n):
    return n > 0


def featureEngineering(dataset):
    dataset.insert(loc=0, column='weekday', value=list(map(getWeekday, dataset['date'])))
    dataset.insert(loc=0, column='day', value=list(map(getDay, dataset['date'])))
    dataset.insert(loc=0, column='month', value=list(map(getMonth, dataset['date'])))
    dataset.insert(loc=0, column='year', value=list(map(getYear, dataset['date'])))
    dataset['age'] = list(map(getAge, dataset['age']))
    dataset['n_contacts_campaign'] = list(map(getCampaign, dataset['n_contacts_campaign']))
    dataset['days_since_last_contact'] = list(map(getDaysLastContact, dataset['days_since_last_contact']))
    dataset['duration'] = list(map(getDuration, dataset['duration']))
    dataset.insert(loc=0, column='contact_before', value=list(map(isContactBefore, dataset['n_contacts_before'])))
    return dataset.drop(columns=['date', 'n_contacts_before'])


dataset = featureEngineering(dataset)

# X = feature data, y = label data
X, y = dataset.iloc[:, :17], dataset['success']

for cat_column in X.columns:
    j_df = pd.DataFrame()

    j_df['yes'] = dataset[dataset['success'] == 'Yes'][cat_column].value_counts()
    j_df['no'] = dataset[dataset['success'] == 'No'][cat_column].value_counts()

    j_df.plot.bar(title=cat_column + ' and success')

print(sorted(Counter(y).items()))

if not skip_fitting:
    # Model, Pipeline and Scoring Initialization

    # Hyperparameter Definition RandomForestClassifier
    hyperparamsRFC = {
        'n_estimators': n_estimators, 'criterion': 'gini', 'max_depth': 50, 'min_samples_split': 5,
        'min_samples_leaf': 25, 'min_weight_fraction_leaf': 0.0, 'max_features': None,
        'max_leaf_nodes': None, 'min_impurity_decrease': 0.0, 'min_impurity_split': None,
        'bootstrap': True, 'oob_score': False, 'n_jobs': -1, 'random_state': 1909,
        'verbose': 1, 'warm_start': False, 'class_weight': None}

    # Preprocessor Definition
    label_features = ['year', 'month', 'day', 'weekday', 'age', 'marital_status', 'education', 'job', 'credit_default',
                      'housing_loan', 'personal_loan', 'communication_type', 'n_contacts_campaign',
                      'days_since_last_contact', 'previous_conversion', 'duration']

    categorical_transformer = Pipeline([
        ('onehotencoder', OneHotEncoder(handle_unknown='ignore')),
    ])

    preprocessor = ColumnTransformer([
        ('categorical_transformer', categorical_transformer, label_features),
    ])

    # Pipeline RandomForestClassifier
    pipeline = make_pipeline(
        preprocessor,
        SMOTE(sampling_strategy='minority', n_jobs=-1),
        RandomForestClassifier(**hyperparamsRFC),
    )

    scorer = make_scorer(f1_score, pos_label='Yes')

    # Evaluation
    # RandomizedSearch RandomForestClassifier
    param_distributions_RS_RFC = {'randomforestclassifier__n_estimators': randint(10, 200),
                                  'randomforestclassifier__max_depth': randint(10, 100),
                                  'randomforestclassifier__min_samples_split': randint(2, 20),
                                  'randomforestclassifier__min_samples_leaf': randint(5, 50),
                                  'randomforestclassifier__max_features': randint(5, 100)}

    search = RandomizedSearchCV(
        pipeline, param_distributions=param_distributions_RS_RFC, n_iter=10, scoring=scorer,
        n_jobs=-1, iid=False, cv=10, random_state=1909, return_train_score=True)

    search = search.fit(X, y)

    training_score = search.cv_results_['mean_train_score'][search.best_index_] * 100
    test_score = search.cv_results_['mean_test_score'][search.best_index_] * 100

    # last result: Mean F1 Score (Training/Test): 67.74/62.67
    print('Mean F1 Score (Training/Test): %.2f/%.2f' % (training_score, test_score))

    # dump model
    joblib.dump(search.best_estimator_, model_dump_file_name)
else:
    # Prediction
    def getYesNo(n):
        if n:
            return 'Yes'
        else:
            return 'No'


    prediction_dataset = featureEngineering(prediction_dataset)

    predictions = list(map(getYesNo, (joblib.load(model_dump_file_name).predict_proba(prediction_dataset) > 0.5)[:, 1]))

    print(sorted(Counter(predictions).items()))

    # Submission Dataset Preparation
    # Your upload to the Online-Campus should contain your written report (the actual seminar paper), this notebook as file as well as the generated submission dataset with your predictions.

    submission = pd.DataFrame(predictions, index=prediction_dataset.index, columns=['prediction'])

    matriculation_number = '******'

    submission.to_csv(f'./submission-{matriculation_number}.csv', index_label='identifier')
